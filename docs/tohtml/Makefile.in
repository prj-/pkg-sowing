ALL: all-redirect
SHELL  = @SHELL@
TOHTML = ../../src/tohtml/tohtml
prefix = @prefix@
wwwdir = @wwwdir@
MKDIR  = @MKDIR@

DOCNAME = tohtml
DOC_FILES =
LATEX = latex

all-redirect: $(DOCNAME).pdf $(DOCNAME)/$(DOCNAME).htm

# Run the first latex into /dev/null to suppress message about missing
# bib citations and forward references
$(DOCNAME).pdf: $(DOCNAME).tex $(DOC_FILES)
	-$(LATEX) $(DOCNAME) 2>&1 >/dev/null </dev/null
	-bibtex $(DOCNAME)
	-$(LATEX) $(DOCNAME) 2>&1 >/dev/null </dev/null
	$(LATEX) $(DOCNAME)
	dvipdfm $(DOCNAME).dvi

$(DOCNAME)/$(DOCNAME).htm: $(DOCNAME).tex $(DOC_FILES)
	$(TOHTML) -default -dosnl $(DOCNAME).tex
	$(TOHTML) -default -dosnl $(DOCNAME).tex

Makefile: Makefile.in ../../config.status
	(cd ../.. && ./config.status --file=docs/$(DOCNAME)/Makefile)

clean:
	rm -f $(DOCNAME).pdf \
		*.aux *.dvi *.toc *.log *.fn *.hux *.err *.blg *.bbl

distclean: clean
	rm -f Makefile

install-web: $(DOCNAME)/$(DOCNAME).htm
	-rm -rf $(wwwdir)/$(DOCNAME)
	cp -rp $(DOCNAME) $(wwwdir)/$(DOCNAME)
install-pdf: $(DOCNAME).pdf
	cp -p $(DOCNAME).pdf $(wwwdir)/$(DOCNAME).pdf

install-prefix:
	if [ ! -d $(wwwdir) ] ; then $(MKDIR) $(wwwdir) ; fi

install: install-prefix install-web install-pdf

.PHONY: clean distclean \
	install-prefix install-web install-pdf install all-redirect
